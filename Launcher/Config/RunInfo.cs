﻿using System;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Serialization;

namespace Launcher.Config
{
  [DataContract(Name = "RunInfo", Namespace = "http://www.bp.com/systems-programming/task-2/config")]
  public class RunInfo
  {
    public RunInfo(long memoryLimit, TimeSpan processorTimeLimit, TimeSpan absolutTimeLimit, int threadCountLimit, int handleCountLimit, long virtualMemoryLimit, long bytesReadLimit, long bytesWrittenLimit)
    {
      MemoryLimit = memoryLimit;
      ProcessorTimeLimit = processorTimeLimit;
      AbsoluteTimeLimit = absolutTimeLimit;
      ThreadCountLimit = threadCountLimit;
      HandleCountLimit = handleCountLimit;
      VirtualMemoryLimit = virtualMemoryLimit;
      BytesReadLimit = bytesReadLimit;
      BytesWrittenLimit = bytesWrittenLimit;
    }

    [DataMember(Name = "AbsoluteTimeLimit", Order = 3)]
    public TimeSpan AbsoluteTimeLimit { get; private set; }

    [DataMember(Name = "BytesReadLimit", Order = 7)]
    public long BytesReadLimit { get; private set; }

    [DataMember(Name = "BytesWrittenLimit", Order = 8)]
    public long BytesWrittenLimit { get; private set; }

    [DataMember(Name = "HandleCountLimit", Order = 5)]
    public int HandleCountLimit { get; private set; }

    [DataMember(Name = "MemoryLimit", Order = 1)]
    public long MemoryLimit { get; private set; }

    [DataMember(Name = "ProcessorTimeLimit", Order = 2)]
    public TimeSpan ProcessorTimeLimit { get; private set; }

    [DataMember(Name = "ThreadCountLimit", Order = 4)]
    public int ThreadCountLimit { get; private set; }

    [DataMember(Name = "VirtualMemoryLimit", Order = 6)]
    public long VirtualMemoryLimit { get; private set; }

    public static RunInfo ReadConfig(string name)
    {
      using (var xr = XmlReader.Create(name))
      {
        var dcs = new DataContractSerializer(typeof(RunInfo));

        return dcs.ReadObject(xr) as RunInfo;
      }
    }

    public void WriteConfig(string name)
    {
      var settings = new XmlWriterSettings()
      {
        Indent = true,
        OmitXmlDeclaration = true,
      };

      using (var xw = XmlWriter.Create(name, settings))
      {
        var dcs = new DataContractSerializer(typeof(RunInfo));

        dcs.WriteObject(xw, this);
      }
    }
  }
}