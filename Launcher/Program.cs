﻿using System;
using System.Diagnostics;
using Launcher;
using System.Runtime.Serialization;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Reflection;
using Monitoring.Monitor;
using Monitoring.Guard;
using Launcher.Config;
using Launcher.Logger;

namespace Launcher
{
  internal class Program
  {
    private static void Main(string[] args)
    {
      Console.Title = "Launcher";

      if (args.Length == 0)
      {
        Console.WriteLine(
          "USAGE\n" +
          "    Launcher <application> [<command line>]\n" +
          "WHERE\n" +
          "    <application>\n" +
          "            The name of the application to start\n" +
          "    <command line>\n" +
          "            Command line arguments to pass to the appllication"
          );
        Console.ReadKey();
        return;
      }

      Console.WriteLine(
        "Launcher v.0.1\n" +
        "Press any key to exit..."
        );

      const string configName = "runinfo.xml";
      const string logName = "processlog.xml";

      var arguments = String.Join(" ", args, 1, args.Length - 1);

      try
      {
        var config = RunInfo.ReadConfig(configName);
        using (var logger = new EventLogger(logName))
        using (var monitor = new Monitor(args[0], arguments))
        {
          Guard guard = new Guard(config.MemoryLimit, config.ProcessorTimeLimit, config.AbsoluteTimeLimit, config.ThreadCountLimit, config.HandleCountLimit, config.VirtualMemoryLimit, config.BytesReadLimit, config.BytesWrittenLimit, 0.9d);
          guard.AttachToMonitor(monitor);
          logger.AttachToGuard(guard);

          monitor.Interval = 1000;
          monitor.Start();

          Console.ReadKey();
        }
      }
      catch (FileNotFoundException)
      {
        Console.WriteLine("\"runinfo.xml\" was not found");
      }
    }
  }
}