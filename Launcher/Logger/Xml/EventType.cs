﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Launcher.Logger.Xml
{
  [DataContract(Namespace = "http://www.bp.com/systems-programming/task-2/logger")]
  public enum EventType
  {
    [EnumMember]
    Notification,

    [EnumMember]
    Warning,

    [EnumMember]
    Error
  }
}