﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Launcher.Logger.Xml
{
  [DataContract(Name = "Stats", Namespace = "http://www.bp.com/systems-programming/task-2/logger")]
  public class StatsRecord
  {
    [DataMember(Name = "AbsoluteTime", Order = 3)]
    public TimeSpan AbsoluteTime { get; set; }

    [DataMember(Name = "BytesRead", Order = 7)]
    public long BytesRead { get; set; }

    [DataMember(Name = "BytesWritten", Order = 8)]
    public long BytesWritten { get; set; }

    [DataMember(Name = "HandleCount", Order = 5)]
    public int HandleCount { get; set; }

    [DataMember(Name = "Memory", Order = 1)]
    public long Memory { get; set; }

    [DataMember(Name = "ProcessorTime", Order = 2)]
    public TimeSpan ProcessorTime { get; set; }

    [DataMember(Name = "ThreadCount", Order = 4)]
    public int ThreadCount { get; set; }

    [DataMember(Name = "VirtualMemory", Order = 6)]
    public long VirtualMemory { get; set; }

    public static explicit operator StatsRecord(Monitoring.Guard.Report report)
    {
      return new StatsRecord()
      {
        Memory = report.Memory,
        ProcessorTime = report.ProcessorTime,
        AbsoluteTime = report.AbsoluteTime,
        ThreadCount = report.ThreadCount,
        HandleCount = report.HandleCount,
        VirtualMemory = report.VirtualMemory,
        BytesRead = report.BytesRead,
        BytesWritten = report.BytesWritten
      };
    }
  }
}