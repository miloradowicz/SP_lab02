﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Launcher.Logger.Xml
{
  [DataContract(Name = "Event", Namespace = "http://www.bp.com/systems-programming/task-2/logger")]
  [KnownType(typeof(StatsRecord))]
  [KnownType(typeof(MessageList))]
  [KnownType(typeof(EventType))]
  public class EventRecord
  {
    [DataMember(Name = "Details", EmitDefaultValue = false, Order = 5)]
    public StatsRecord Details { get; set; }

    [DataMember(Name = "Time", Order = 2)]
    public DateTime EventTime { get; set; }

    [DataMember(Name = "Type", Order = 1)]
    public EventType EventType { get; set; }

    [DataMember(Name = "ExitCode", EmitDefaultValue = false, Order = 7)]
    public int? ExitCode { get; set; }

    [DataMember(Name = "Messages", EmitDefaultValue = false, Order = 6)]
    public MessageList Messages { get; set; }

    [DataMember(Name = "PID", Order = 3)]
    public int ProcessId { get; set; }

    [DataMember(Name = "Process", Order = 4)]
    public string ProcessName { get; set; }
  }
}