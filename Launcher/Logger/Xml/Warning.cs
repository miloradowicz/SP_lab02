﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Launcher
{
  [DataContract(Namespace = "http://www.bp.com/systems-programming/task-2/logger")]
  public enum Warning
  {
    [EnumMember(Value = "Approaching memory limit")]
    MemoryLimitApproaching,

    [EnumMember(Value = "Approaching processor time limit")]
    ProcessorTimeLimitApproaching,

    [EnumMember(Value = "Approaching absolute time limit")]
    AbsoluteTimeLimitApproaching,

    [EnumMember(Value = "Approaching thread count limit")]
    ThreadCountLimitApproaching,

    [EnumMember(Value = "Approaching handle count limit")]
    HandleCountLimitApproaching,

    [EnumMember(Value = "Approaching virtual memory limit")]
    VirtualMemoryLimitApproaching,

    [EnumMember(Value = "Approaching read bytes limit")]
    BytesReadLimitApproaching,

    [EnumMember(Value = "Approaching write bytes limit")]
    BytesWrittenLimitApproaching
  }
}