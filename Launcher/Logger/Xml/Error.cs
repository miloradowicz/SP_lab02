﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Launcher
{
  [DataContract(Namespace = "http://www.bp.com/systems-programming/task-2/logger")]
  public enum Error
  {
    [EnumMember(Value = "Memory limit exceeded")]
    MemoryLimitExceeded,

    [EnumMember(Value = "Processor time limit exceeded")]
    ProcessorTimeLimitExceeded,

    [EnumMember(Value = "Absolute time limit exceeded")]
    AbsoluteTimeLimitExceeded,

    [EnumMember(Value = "Thread count limit exceeded")]
    ThreadCountLimitExceeded,

    [EnumMember(Value = "Handle count limit exceeded")]
    HandleCountLimitExceeded,

    [EnumMember(Value = "Virtual memory limit exceeded")]
    VirtualMemoryLimitExceeded,

    [EnumMember(Value = "Read bytes limit exceeded")]
    BytesReadLimitExceeded,

    [EnumMember(Value = "Write bytes limit exceeded")]
    BytesWrittenLimitExceeded
  }
}