﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Launcher.Logger.Xml
{
  [CollectionDataContract(Name = "Event", Namespace = "http://www.bp.com/systems-programming/task-2/logger")]
  [KnownType(typeof(EventRecord))]
  public class EventJournal : List<EventRecord> { }
}