﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Launcher.Logger.Xml
{
  public struct TimeSpan : IXmlSerializable
  {
    public static implicit operator TimeSpan(System.TimeSpan timespan) => new TimeSpan { _value = timespan };

    public XmlSchema GetSchema()
    {
      return null;
    }

    public void ReadXml(XmlReader reader)
    {
      throw new NotImplementedException();
    }

    public void WriteXml(XmlWriter writer)
    {
      writer.WriteValue(_value.TotalSeconds);
    }

    public static implicit operator System.TimeSpan(TimeSpan timespan) => timespan._value;

    private System.TimeSpan _value;
  }
}