﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Launcher
{
  [DataContract(Namespace = "http://www.bp.com/systems-programming/task-2/logger")]
  public enum Notification
  {
    [EnumMember(Value = "Process started")]
    ProcessStarted,

    [EnumMember(Value = "Process exited")]
    ProcessExited,

    [EnumMember(Value = "Process terminated")]
    ProcessTerminated
  }
}