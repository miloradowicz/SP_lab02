﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Launcher.Logger.Xml
{
  [CollectionDataContract(Name = "Message", Namespace = "http://www.bp.com/systems-programming/task-2/logger")]
  [KnownType(typeof(Warning))]
  [KnownType(typeof(Error))]
  [KnownType(typeof(Notification))]
  public class MessageList : List<object>
  {
  }
}