﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Launcher.Logger.Xml;
using Monitoring.Guard;

namespace Launcher.Logger
{
  public class EventLogger : IDisposable
  {
    public EventLogger(string name)
    {
      _dcs = new DataContractSerializer(typeof(EventJournal));
      _settings = new XmlWriterSettings()
      {
        Indent = true,
        NamespaceHandling = NamespaceHandling.OmitDuplicates,
        NewLineChars = "\n",
        NewLineHandling = NewLineHandling.Replace,
        OmitXmlDeclaration = true,
      };
      _xw = XmlWriter.Create(name, _settings);
    }

    public void AddRecord(EventRecord e)
    {
      _dcs.WriteObject(_xw, e);
      _xw.Flush();
    }

    public void AttachToGuard(IGuard guard)
    {
      _xw.WriteStartDocument(false);
      _xw.WriteStartElement(localName: "Journal", ns: "http://www.bp.com/systems-programming/task-2/logger");

      guard.ConstraintsChecked += WriteEvent;
      _guard = guard;
    }

    public void DetachFromGuard()
    {
      _xw.WriteEndElement();
      _xw.WriteEndDocument();

      _guard.ConstraintsChecked -= WriteEvent;
      _guard = null;
    }

    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    public void WriteEvent(object sender, ReportEventArgs e)
    {
      Report report = e.Report;

      bool fail = report.MemoryLimitFail || report.ProcessorTimeLimitFail || report.AbsoluteTimeLimitFail || report.ThreadCountLimitFail || report.HandleCountLimitFail || report.VirtualMemoryLimitFail || report.BytesReadLimitFail || report.BytesWrittenLimitFail;
      bool alert = report.MemoryLimitAlert || report.ProcessorTimeLimitAlert || report.AbsoluteTimeLimitAlert || report.ThreadCountLimitAlert || report.HandleCountLimitAlert || report.VirtualMemoryLimitAlert || report.BytesReadLimitAlert || report.BytesWrittenLimitAlert;

      EventRecord eventRecord = new EventRecord()
      {
        EventType =
        fail ? EventType.Error :
        alert ? EventType.Warning :
        EventType.Notification,
        EventTime = DateTime.Now,
        ProcessId = report.ProcessId,
        ProcessName = report.ProcessName,
        Messages = new MessageList(),
        Details = report.Status == Status.Started || report.Status == Status.Exited || report.Status == Status.Stopped ? null : (StatsRecord)report
      };

      switch (report.Status)
      {
        case Status.Started:
        {
          eventRecord.Messages.Add(Notification.ProcessStarted);
          break;
        }

        case Status.Exited:
        {
          eventRecord.Messages.Add(Notification.ProcessExited);
          eventRecord.Messages.Add($"Exit Code: {report.ExitCode}");
          break;
        }

        case Status.Stopped:
        {
          eventRecord.Messages.Add(Notification.ProcessTerminated);
          break;
        }

        default:
        case Status.Running:
        {
          if (fail)
          {
            if (report.MemoryLimitFail)
              eventRecord.Messages.Add(Error.MemoryLimitExceeded);
            if (report.ProcessorTimeLimitFail)
              eventRecord.Messages.Add(Error.ProcessorTimeLimitExceeded);
            if (report.AbsoluteTimeLimitFail)
              eventRecord.Messages.Add(Error.AbsoluteTimeLimitExceeded);
            if (report.ThreadCountLimitFail)
              eventRecord.Messages.Add(Error.ThreadCountLimitExceeded);
            if (report.HandleCountLimitFail)
              eventRecord.Messages.Add(Error.HandleCountLimitExceeded);
            if (report.VirtualMemoryLimitFail)
              eventRecord.Messages.Add(Error.VirtualMemoryLimitExceeded);
            if (report.BytesReadLimitFail)
              eventRecord.Messages.Add(Error.BytesReadLimitExceeded);
            if (report.BytesWrittenLimitFail)
              eventRecord.Messages.Add(Error.BytesWrittenLimitExceeded);
          }
          else if (alert)
          {
            if (report.MemoryLimitAlert)
              eventRecord.Messages.Add(Warning.MemoryLimitApproaching);
            if (report.ProcessorTimeLimitAlert)
              eventRecord.Messages.Add(Warning.ProcessorTimeLimitApproaching);
            if (report.AbsoluteTimeLimitAlert)
              eventRecord.Messages.Add(Warning.AbsoluteTimeLimitApproaching);
            if (report.ThreadCountLimitAlert)
              eventRecord.Messages.Add(Warning.ThreadCountLimitApproaching);
            if (report.HandleCountLimitAlert)
              eventRecord.Messages.Add(Warning.HandleCountLimitApproaching);
            if (report.VirtualMemoryLimitAlert)
              eventRecord.Messages.Add(Warning.VirtualMemoryLimitApproaching);
            if (report.BytesReadLimitAlert)
              eventRecord.Messages.Add(Warning.BytesReadLimitApproaching);
            if (report.BytesWrittenLimitAlert)
              eventRecord.Messages.Add(Warning.BytesWrittenLimitApproaching);
          }
          break;
        }
      }

      AddRecord(eventRecord);
    }

    protected virtual void Dispose(bool disposing)
    {
      if (!_disposed)
      {
        if (disposing)
        {
          DetachFromGuard();

          _xw.Flush();
          _xw.Close();
        }

        _disposed = true;
      }
    }

    private DataContractSerializer _dcs;
    private bool _disposed;
    private IGuard _guard;
    private XmlWriterSettings _settings;
    private XmlWriter _xw;
  }
}