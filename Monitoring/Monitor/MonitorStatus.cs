﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monitoring.Monitor
{
  public enum MonitorStatus
  {
    Started,
    Running,
    Paused,
    Stopped,
    Exited
  }
}