﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monitoring.Monitor
{
  public class SnapshotEventArgs : EventArgs
  {
    public Snapshot Snapshot { get; }

    internal SnapshotEventArgs(Snapshot snapshot) : base() => Snapshot = snapshot;
  }
}