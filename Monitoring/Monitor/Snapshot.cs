﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monitoring.Monitor
{
  public class Snapshot
  {
    public long BytesRead { get; internal set; }
    public long BytesWritten { get; internal set; }
    public int ExitCode { get; internal set; }
    public int HandleCount { get; internal set; }
    public long Memory { get; internal set; }
    public int ProcessId { get; internal set; }
    public string ProcessName { get; internal set; }
    public TimeSpan ProcessorTime { get; internal set; }
    public DateTime StartTime { get; internal set; }
    public MonitorStatus Status { get; internal set; }
    public int ThreadCount { get; internal set; }
    public long VirtualMemory { get; internal set; }

    internal Snapshot()
    {
      Status = MonitorStatus.Stopped;
      BytesRead = 0;
      BytesWritten = 0;
      ExitCode = 0;
      HandleCount = 0;
      Memory = 0;
      ProcessId = 0;
      ProcessName = String.Empty;
      ProcessorTime = TimeSpan.Zero;
      StartTime = DateTime.MinValue;
      ThreadCount = 0;
      VirtualMemory = 0;
    }
  }
}