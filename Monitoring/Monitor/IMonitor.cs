﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monitoring.Monitor
{
  public interface IMonitor
  {
    event EventHandler<SnapshotEventArgs> EventAdded;

    event EventHandler<SnapshotEventArgs> MonitorTick;

    int Interval { get; set; }

    ReadOnlyObservableCollection<Snapshot> Journal { get; }

    void Start();

    void Stop();
  }
}