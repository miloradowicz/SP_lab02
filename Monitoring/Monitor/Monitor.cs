﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Timers;

namespace Monitoring.Monitor
{
  public class Monitor : IDisposable, IMonitor
  {
    public Monitor()
    {
      _isValid = false;
      _reporting = false;
      _timer = null;
      Interval = 3000;

      _journal = new ObservableCollection<Snapshot>();
      Journal = new ReadOnlyObservableCollection<Snapshot>(_journal);
      _reportQueue = new ConcurrentQueue<Snapshot>();

      _timer = new Timer()
      {
        SynchronizingObject = System.Diagnostics.Process.GetCurrentProcess().SynchronizingObject
      };
      _process = new Process();
      _journal.CollectionChanged += PatchThrough;
    }

    public Monitor(string name) : this()
    {
      _process.StartInfo.FileName = name;
    }

    public Monitor(string name, string args) : this()
    {
      _process.StartInfo.FileName = name;
      _process.StartInfo.Arguments = args;
    }

    public event EventHandler<SnapshotEventArgs> EventAdded;

    public event EventHandler<SnapshotEventArgs> MonitorTick;

    public int Interval { get; set; }
    public ReadOnlyObservableCollection<Snapshot> Journal { get; private set; }

    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    public void Start()
    {
      if (_isValid)
        throw new InvalidOperationException("The process has already been started");

      if (!_process.Start())
        throw new InvalidOperationException("Could not start the process");

      _isValid = true;
      _reporting = true;

      Report(ReportReason.Started);

      _timer.AutoReset = true;
      _timer.Elapsed += TimerTick;
      _timer.Interval = Interval;
      _timer.Start();
    }

    public void Stop()
    {
      _timer.Stop();

      if (_isValid && !_process.HasExited)
      {
        _process.Kill();

        Report(ReportReason.Stopped);
      }
    }

    protected virtual void Dispose(bool disposing)
    {
      if (!disposedValue)
      {
        if (disposing)
        {
          _timer.Stop();
          _timer.Dispose();

          if (_isValid && !_process.HasExited)
          {
            _process.Kill();

            Report(ReportReason.Stopped);
          }
          _process.Dispose();
        }

        _journal.CollectionChanged -= PatchThrough;
        disposedValue = true;
      }
    }

    private bool _isValid;
    private object _j = new object();
    private ObservableCollection<Snapshot> _journal;
    private Process _process;
    private bool _reporting;
    private ConcurrentQueue<Snapshot> _reportQueue;
    private Timer _timer;

    private bool disposedValue;

    private void PatchThrough(object sender, NotifyCollectionChangedEventArgs e)
    {
      foreach (Snapshot s in e.NewItems)
        _reportQueue.Enqueue(s);
    }

    private void Report(ReportReason reason)
    {
      lock (_j)
      {
        if (_isValid && _process.HasExited)
          _timer.Stop();

        if (_reporting)
        {
          Snapshot snapshot = TakeSnapshot(reason);
          _journal.Add(snapshot);

          while (!_reportQueue.IsEmpty)
          {
            if (_reportQueue.TryDequeue(out snapshot))
              EventAdded?.Invoke(this, new SnapshotEventArgs(snapshot));
          }

          MonitorTick?.Invoke(this, new SnapshotEventArgs(snapshot));

          _reporting = !_process.HasExited;
        }
      }
    }

    private Snapshot TakeSnapshot(ReportReason reason)
    {
      if (!_isValid)
        throw new ArgumentException("Process does not exist");

      Snapshot snapshot = new Snapshot()
      {
        ProcessId = _process.Id,
        ProcessName = _process.ProcessName,
        StartTime = _process.StartTime,
        ProcessorTime = _process.TotalProcessorTime
      };

      if (_process.HasExited)
      {
        snapshot.Status = reason == ReportReason.Stopped ? MonitorStatus.Stopped : MonitorStatus.Exited;
        snapshot.ExitCode = _process.ExitCode;
      }
      else
      {
        snapshot.Status = reason == ReportReason.Started ? MonitorStatus.Started : MonitorStatus.Running;
        snapshot.Memory = _process.WorkingSet64;
        snapshot.ThreadCount = _process.Threads.Count;
        snapshot.HandleCount = _process.HandleCount;
        snapshot.VirtualMemory = _process.VirtualMemorySize64;

        Win32Helper.IoCounters ioCounters = new Win32Helper.IoCounters();
        if (Win32Helper.GetProcessIoCounters(_process.Handle, ioCounters))
        {
          snapshot.BytesRead = ioCounters.ReadTransferCount;
          snapshot.BytesWritten = ioCounters.WriteTransferCount;
        }
        else
        {
          snapshot.BytesRead = 0;
          snapshot.BytesWritten = 0;
        }
      }

      return snapshot;
    }

    private void TimerTick(object sender, ElapsedEventArgs e)
    {
      Report(ReportReason.Normal);
    }
  }
}