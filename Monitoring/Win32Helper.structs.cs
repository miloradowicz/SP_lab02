﻿using Microsoft.Win32.SafeHandles;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

#if X64
using SysInt = System.Int64;
#else

using SysInt = System.Int32;

#endif

#if DEBUG
[assembly: InternalsVisibleTo("Launcher")]
#endif

namespace Monitoring
{
  internal static partial class Win32Helper
  {
    public const int MaxPath = 260;

    public const SysInt InvalidHandleValue = (SysInt)(-1L);

    public enum CreationFlags : int
    {
      BreakawayFromJob = 0x01000000,
      DefaultErrorMode = 0x04000000,
      NewConsole = 0x00000010,
      NewProcessGroup = 0x00000200,
      NoWindow = 0x08000000,
      ProtectedProcess = 0x00040000,
      PreserveCodeAuthzLevel = 0x02000000,
      SecureProcess = 0x00400000,
      SeparateWowVdm = 0x00000800,
      SharedWowVdm = 0x00001000,
      Suspended = 0x00000004,
      UnicodeEnvironment = 0x00000400,
      DebugOnlyThisProcess = 0x00000002,
      DebugProcess = 0x00000001,
      DetachedProcess = 0x00000008,
      ExtendedStartupPresent = 0x00080000,
      InheritParentAffinity = 0x00010000,
    }

    public enum Th32CsFlags : int
    {
      Inherit = unchecked((int)
                      0x80000000),

      SnapAll = SnapHeapList | SnapModule | SnapProcess | SnapThread,
      SnapHeapList = 0x00000001,
      SnapModule = 0x00000008,
      SnapModule32 = 0x00000010,
      SnapProcess = 0x00000002,
      SnapThread = 0x00000004,
    }

    [StructLayout(LayoutKind.Sequential)]
    public class MemoryStatusEx
    {
      public int length;
      public int memoryLoad;
      public long totalPhys;
      public long availPhys;
      public long totalPageFile;
      public long availPageFile;
      public long totalVirtual;
      public long availVirtual;
      public long availExtendedVirtual;

      public MemoryStatusEx() => length = Marshal.SizeOf(this);
    }

    public class SnapshotHandle : SafeHandleMinusOneIsInvalid
    {
      public SnapshotHandle() : base(true) => SetHandle(handle);

      public SnapshotHandle(IntPtr handle) : base(true) => SetHandle(handle);

      protected override bool ReleaseHandle() => CloseHandle(handle);
    }

    [StructLayout(LayoutKind.Sequential)]
    public class Filetime
    {
      public long ticks;

      public DateTime DateTime
      {
        get => new DateTime(ticks);
      }

      public TimeSpan TimeSpan
      {
        get => new TimeSpan(ticks);
      }
    }

    [StructLayout(LayoutKind.Sequential)]
    public class IoCounters
    {
      public long ReadOperationCount;
      public long WriteOperationCount;
      public long OtherOperationCount;
      public long ReadTransferCount;
      public long WriteTransferCount;
      public long OtherTransferCount;
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
    public class ProcessEntry32
    {
      public int size;
      public int usage;
      public int processId;
      public SysInt defaultHeapId;
      public int moduleId;
      public int threads;
      public int parentProcessId;
      public int priClassBase;
      public int flags;

      [MarshalAs(UnmanagedType.ByValTStr, SizeConst = MaxPath)]
      public string exeFile;

      public ProcessEntry32() => size = Marshal.SizeOf(this);
    }

    [StructLayout(LayoutKind.Sequential)]
    public class ProcessInformation
    {
      public IntPtr process;
      public IntPtr thread;
      public int processId;
      public int threadId;
    }

    [StructLayout(LayoutKind.Sequential)]
    public class ProcessMemoryCountersEx
    {
      public int cb;
      public int pageFaultCount;
      public SysInt peakWorkingSetSize;
      public SysInt workingSetSize;
      public SysInt quotaPeakPagedPoolUsage;
      public SysInt quotaPagedPoolUsage;
      public SysInt quotaPeakNonPagedPoolUsage;
      public SysInt quotaNonPagedPoolUsage;
      public SysInt pagefileUsage;
      public SysInt peakPageFileUsage;
      public SysInt privateUsage;

      public ProcessMemoryCountersEx() => cb = Marshal.SizeOf(this);
    }

    [StructLayout(LayoutKind.Sequential)]
    public class SecurityAttributes
    {
      public int length;
      public IntPtr securityDescriptor;
      public bool inheritHandle;
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
    public class StartupInfo
    {
      public int cb;
      public string @_;
      public string desktop;
      public string title;
      public int x;
      public int y;
      public int xSize;
      public int ySize;
      public int xCountChars;
      public int yCountChars;
      public int fillAttribute;
      public int flags;
      public short showWindow;
      public short @__;
      public IntPtr @___;
      public IntPtr stdInput;
      public IntPtr stdOutput;
      public IntPtr stdError;

      public StartupInfo() => cb = Marshal.SizeOf(this);
    }
  }
}