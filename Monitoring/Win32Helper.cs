﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

#if X64
using SysInt = System.Int64;
#else

using SysInt = System.Int32;

#endif

#if DEBUG
[assembly: InternalsVisibleTo("Launcher")]
#endif

namespace Monitoring
{
  internal static partial class Win32Helper
  {
    [DllImport("kernel32.dll")]
    [return: MarshalAs(UnmanagedType.Bool)]
    public static extern bool CloseHandle(
      [In] IntPtr handle
      );

    [DllImport("kernel32.dll", SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    public static extern bool CreateProcess(
      [In] string applicationName,
      [In] string commandName,
      [In] SecurityAttributes processAttributes,
      [In] SecurityAttributes threadAttributes,
      [In] bool inhertiHandles,
      [In] CreationFlags creationFlags,
      [In] IntPtr environment,
      [In] string currentDirectory,
      [In] StartupInfo startupInfo,
      [Out] ProcessInformation processInformation
      );

    [DllImport("kernel32.dll")]
    [return: MarshalAs(UnmanagedType.Bool)]
    public static extern bool GetExitCodeProcess(
      [In] IntPtr process,
      [Out] out int exitCode
      );

    [DllImport("kernel32.dll")]
    [return: MarshalAs(UnmanagedType.U4)]
    public static extern int GetLastError();

    [DllImport("kernel32.dll", SetLastError = true)]
    public static extern void GetStartupInfo(
      [Out] StartupInfo startupInfo
      );

    [DllImport("kernel32.dll")]
    [return: MarshalAs(UnmanagedType.Bool)]
    public static extern bool TerminateProcess(
      [In] IntPtr process,
      [In] int exitCode
      );

    [DllImport("psapi.dll", SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    public static extern bool GetProcessMemoryInfo(
      [In] IntPtr process,
      [Out] ProcessMemoryCountersEx memCounters,
      [In] int cb);

    [DllImport("kernel32.dll", SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    public static extern bool GetProcessHandleCount(
      [In] IntPtr process,
      [In, Out] ref IntPtr handleCount
      );

    [DllImport("kernel32.dll", SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    public static extern bool GetProcessIoCounters(
      [In] IntPtr process,
      [Out] IoCounters ioCounters
      );

    [DllImport("kernel32.dll", SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    public static extern bool GetProcessTimes(
      [In] IntPtr process,
      [Out] out long creationTime,
      [Out] out long exitTime,
      [Out] out long kernelTime,
      [Out] out long userTime
      );

    [DllImport("kernel32.dll", SetLastError = true)]
    public static extern SnapshotHandle CreateToolhelp32Snapshot(
      [In] Th32CsFlags flags,
      [In] int processId
      );

    [DllImport("kernel32.dll", SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    public static extern bool Process32First(
      [In] SnapshotHandle snapshot,
      [In, Out] ProcessEntry32 pe
      );

    [DllImport("kernel32.dll", SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    public static extern bool Process32Next(
      [In] SnapshotHandle snapshot,
      [In, Out] ProcessEntry32 pe
      );

    [DllImport("kernel32.dll", SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    public static extern bool GlobalMemoryStatusEx(
      [In, Out] MemoryStatusEx buffer
      );

    [DllImport("kernel32.dll", SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    public static extern bool GetProcessHandleCount(
      IntPtr process,
      [In, Out] ref int handleCount
      );
  }
}