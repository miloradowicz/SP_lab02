﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monitoring.Guard
{
  public class Guard : IGuard
  {
    public Guard(long memoryLimit, TimeSpan processorTimeLimit, TimeSpan absouluteTimeLimit, int threadCountLimit, int handleCountLimit, long virtualMemoryLimit, long bytesReadLimit, long bytesWrittenLimit, double alertThreshold)
    {
      MemoryLimit = memoryLimit;
      ProcessorTimeLimit = processorTimeLimit;
      AbsoluteTimeLimit = absouluteTimeLimit;
      ThreadCountLimit = threadCountLimit;
      HandleCountLimit = handleCountLimit;
      VirtualMemoryLimit = virtualMemoryLimit;
      BytesReadLimit = bytesReadLimit;
      BytesWrittenLimit = bytesWrittenLimit;
      AlertThreshold = alertThreshold;

      _kill = false;
    }

    public event EventHandler<ReportEventArgs> ConstraintsChecked;

    public TimeSpan AbsoluteTimeLimit { get; private set; }
    public double AlertThreshold { get; private set; }
    public long BytesReadLimit { get; private set; }
    public long BytesWrittenLimit { get; private set; }
    public int HandleCountLimit { get; private set; }
    public long MemoryLimit { get; private set; }
    public TimeSpan ProcessorTimeLimit { get; private set; }
    public int ThreadCountLimit { get; private set; }
    public long VirtualMemoryLimit { get; private set; }

    public void AttachToMonitor(Monitor.IMonitor monitor)
    {
      monitor.EventAdded += GuardTick;
      _monitor = monitor;
    }

    public void DetachFromMonitor()
    {
      _monitor.EventAdded -= GuardTick;
      _monitor = null;
    }

    private object _j = new object();
    private bool _kill;
    private Monitor.IMonitor _monitor;

    private Report CheckConstraints(Monitor.Snapshot snapshot)
    {
      Report report = new Report(snapshot);

      if (report.Status == Status.Running)
      {
        report.MemoryLimitFail = report.Memory >= MemoryLimit;
        report.MemoryLimitAlert = !report.MemoryLimitFail && report.Memory >= AlertThreshold * MemoryLimit;

        report.ProcessorTimeLimitFail = report.ProcessorTime >= ProcessorTimeLimit;
        report.ProcessorTimeLimitAlert = !report.ProcessorTimeLimitFail && report.ProcessorTime >= new TimeSpan((long)(AlertThreshold * ProcessorTimeLimit.Ticks));

        report.AbsoluteTimeLimitFail = report.AbsoluteTime >= AbsoluteTimeLimit;
        report.AbsoluteTimeLimitAlert = !report.AbsoluteTimeLimitFail && report.AbsoluteTime >= new TimeSpan((long)(AlertThreshold * AbsoluteTimeLimit.Ticks));

        report.ThreadCountLimitFail = report.ThreadCount >= ThreadCountLimit;
        report.ThreadCountLimitAlert = !report.ThreadCountLimitFail && report.ThreadCount >= AlertThreshold * ThreadCountLimit;

        report.HandleCountLimitFail = report.HandleCount >= HandleCountLimit;
        report.HandleCountLimitAlert = !report.HandleCountLimitFail && report.HandleCount >= AlertThreshold * HandleCountLimit;

        report.VirtualMemoryLimitFail = report.VirtualMemory >= VirtualMemoryLimit;
        report.VirtualMemoryLimitAlert = !report.VirtualMemoryLimitFail && report.VirtualMemory >= AlertThreshold * VirtualMemoryLimit;

        report.BytesReadLimitFail = report.BytesRead >= BytesReadLimit;
        report.BytesReadLimitAlert = !report.BytesReadLimitFail && report.BytesRead >= AlertThreshold * BytesReadLimit;

        report.BytesWrittenLimitFail = report.BytesWritten >= BytesWrittenLimit;
        report.BytesWrittenLimitAlert = !report.BytesWrittenLimitFail && report.BytesWritten >= AlertThreshold * BytesWrittenLimit;

        _kill = report.MemoryLimitFail || report.ProcessorTimeLimitFail || report.AbsoluteTimeLimitFail || report.ThreadCountLimitFail || report.HandleCountLimitFail || report.VirtualMemoryLimitFail || report.BytesReadLimitFail || report.BytesWrittenLimitFail;
      }

      return report;
    }

    private void GuardTick(object sender, Monitor.SnapshotEventArgs e)
    {
      lock (_j)
      {
        if (_monitor == null)
          return;

        Report report = CheckConstraints(e.Snapshot);

        ConstraintsChecked?.Invoke(this, new ReportEventArgs(report));

        if (_kill)
          _monitor?.Stop();

        if (e.Snapshot.Status == Monitor.MonitorStatus.Stopped)
          DetachFromMonitor();
      }
    }
  }
}