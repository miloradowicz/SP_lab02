﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monitoring.Guard
{
  public class ReportEventArgs : EventArgs
  {
    public ReportEventArgs(Report report) => Report = report;

    public Report Report { get; }
  }
}