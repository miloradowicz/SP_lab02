﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monitoring.Guard
{
  public class Report
  {
    public TimeSpan AbsoluteTime { get; internal set; }

    public bool AbsoluteTimeLimitAlert { get; internal set; }

    public bool AbsoluteTimeLimitFail { get; internal set; }

    public long BytesRead { get; internal set; }

    public bool BytesReadLimitAlert { get; internal set; }

    public bool BytesReadLimitFail { get; internal set; }

    public long BytesWritten { get; internal set; }

    public bool BytesWrittenLimitAlert { get; internal set; }

    public bool BytesWrittenLimitFail { get; internal set; }

    public int ExitCode { get; internal set; }

    public int HandleCount { get; internal set; }

    public bool HandleCountLimitAlert { get; internal set; }

    public bool HandleCountLimitFail { get; internal set; }

    public long Memory { get; internal set; }

    public bool MemoryLimitAlert { get; internal set; }

    public bool MemoryLimitFail { get; internal set; }

    public int ProcessId { get; internal set; }

    public string ProcessName { get; internal set; }

    public TimeSpan ProcessorTime { get; internal set; }

    public bool ProcessorTimeLimitAlert { get; internal set; }

    public bool ProcessorTimeLimitFail { get; internal set; }

    public DateTime StartTime { get; internal set; }

    public Status Status { get; internal set; }

    public int ThreadCount { get; internal set; }

    public bool ThreadCountLimitAlert { get; internal set; }

    public bool ThreadCountLimitFail { get; internal set; }

    public long VirtualMemory { get; internal set; }

    public bool VirtualMemoryLimitAlert { get; internal set; }

    public bool VirtualMemoryLimitFail { get; internal set; }

    internal Report(Monitor.Snapshot snapshot)
    {
      MemoryLimitAlert = false;
      MemoryLimitFail = false;
      ProcessorTimeLimitAlert = false;
      ProcessorTimeLimitFail = false;
      AbsoluteTimeLimitAlert = false;
      AbsoluteTimeLimitFail = false;
      ThreadCountLimitAlert = false;
      ThreadCountLimitFail = false;
      HandleCountLimitAlert = false;
      HandleCountLimitFail = false;
      VirtualMemoryLimitAlert = false;
      VirtualMemoryLimitFail = false;
      BytesReadLimitAlert = false;
      BytesReadLimitFail = false;
      BytesWrittenLimitAlert = false;
      BytesWrittenLimitFail = false;
      Status =
        snapshot.Status == Monitor.MonitorStatus.Started ?
        Status.Started :
        snapshot.Status == Monitor.MonitorStatus.Running ?
        Status.Running :
        snapshot.Status == Monitor.MonitorStatus.Paused ?
        Status.Paused :
        snapshot.Status == Monitor.MonitorStatus.Stopped ?
        Status.Stopped :
        snapshot.Status == Monitor.MonitorStatus.Exited ?
        Status.Exited : default(Status);
      AbsoluteTime = DateTime.Now - snapshot.StartTime;
      BytesRead = snapshot.BytesRead;
      BytesWritten = snapshot.BytesWritten;
      ExitCode = snapshot.ExitCode;
      HandleCount = snapshot.HandleCount;
      Memory = snapshot.Memory;
      ProcessId = snapshot.ProcessId;
      ProcessName = snapshot.ProcessName;
      ProcessorTime = snapshot.ProcessorTime;
      StartTime = snapshot.StartTime;
      ThreadCount = snapshot.ThreadCount;
      VirtualMemory = snapshot.VirtualMemory;
    }
  }
}