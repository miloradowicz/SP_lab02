﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monitoring.Guard
{
  public enum Status
  {
    Started,
    Running,
    Paused,
    Stopped,
    Exited
  }
}