﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monitoring.Guard
{
  public interface IGuard
  {
    event EventHandler<ReportEventArgs> ConstraintsChecked;

    TimeSpan AbsoluteTimeLimit { get; }
    double AlertThreshold { get; }
    long BytesReadLimit { get; }
    long BytesWrittenLimit { get; }
    int HandleCountLimit { get; }
    long MemoryLimit { get; }
    TimeSpan ProcessorTimeLimit { get; }
    int ThreadCountLimit { get; }
    long VirtualMemoryLimit { get; }
  }
}